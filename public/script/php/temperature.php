<?php

use Src\Core\Sensor\Sensor;

define('ROOT',$_SERVER['DOCUMENT_ROOT']);
header('Location: 192.168.1.19/public/scripts/php/temperature.php');
require ROOT.'/vendor/autoload.php';

$sensor = new Sensor();

echo ($sensor->parseJson($sensor->exec([
    'path' => ROOT.'/public/script/C',
    'script' => 'fdht'
])));
