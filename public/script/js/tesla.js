/*
* TESLA HUD BY Tameem Imamdad timamdad@hawk.iit.edu
GitHub: https://github.com/tameemi/tesla-speedometer
*/
let servSpeed = 0;
let servRpm = 0;

let dev = false;
//let t0 = 0;
//let t1 = 0;

var c = document.getElementById("canvas");
c.width = 500;
c.height = 500;

var ctx = c.getContext("2d");

//Rescale the size
ctx.scale(1,1);
var speedGradient = ctx.createLinearGradient(0, 500, 0, 0);
speedGradient.addColorStop(0, '#00b8fe');
speedGradient.addColorStop(1, '#00b8fe');

var rpmGradient = ctx.createLinearGradient(0, 500, 0, 0);
rpmGradient.addColorStop(0, '#f7b733');
rpmGradient.addColorStop(1, '#fc4a1a');
//rpmGradient.addColorStop(1, '#EF4836');

function speedNeedle(rotation) {
    ctx.lineWidth = 2;

    ctx.save();
    ctx.translate(250, 250);
    ctx.rotate(rotation);
    ctx.strokeRect(-130 / 2 + 170, -1 / 2, 135, 1);
    ctx.restore();

    rotation += Math.PI / 180;
}

function rpmNeedle(rotation) {
    ctx.lineWidth = 2;

    ctx.save();
    ctx.translate(250, 250);
    ctx.rotate(rotation);
    ctx.strokeRect(-130 / 2 + 170, -1 / 2, 135, 1);
    ctx.restore();

    rotation += Math.PI / 180;
}

function drawMiniNeedle(rotation, width, speed) {
    ctx.lineWidth = width;

    ctx.save();
    ctx.translate(250, 250);
    ctx.rotate(rotation);
    ctx.strokeStyle = "#b1b1b1";
    ctx.fillStyle = "#b1b1b1";
    ctx.strokeRect(-20 / 2 + 220, -1 / 2, 20, 1);
    ctx.restore();

    let x = (250 + 180 * Math.cos(rotation));
    let y = (250 + 180 * Math.sin(rotation));
    if (speed === 100){
        ctx.font = "700 48px Open Sans";
        ctx.fillText(speed, 245, 100);
    }else{
        ctx.font = "700 20px Open Sans";
        ctx.fillText(speed, x, y);

    }


    rotation += Math.PI / 180;
}

function calculateSpeedAngle(x, a, b) {
    let degree = (a - b) * (x) + b;
    let radian = (degree * Math.PI) / 180;
    return radian <= 1.45 ? radian : 1.45;
}

// function calculateRPMAngle(x, a, b) {
//     let degree = (a - b) * (x) + b;
//     let radian = (degree * Math.PI) / 180;
//     return radian <= 1.45 ? radian : 1.45;
// }

function calculateRPMAngle(x, a, b) {
    let degree = (a - b) * (x) + b;
    let radian = (degree * Math.PI) / 180;
    return radian >= -0.46153862656807704 ? radian : -0.46153862656807704;
}

function drawSpeedo(speed, gear, rpm, topSpeed) {
    if (speed == undefined) {
        return false;
    } else {
        speed = Math.floor(speed);
        rpm = rpm * 10;
    }

    ctx.clearRect(0, 0, 500, 500);

    ctx.beginPath();
    ctx.fillStyle = 'rgba(0, 0, 0, 0.9)';
    ctx.arc(250, 250, 240, 0, 2 * Math.PI);
    ctx.fill();
    ctx.save();
    ctx.restore();
    ctx.fillStyle = "#FFF";
    ctx.stroke();

    ctx.beginPath();
    ctx.strokeStyle = "#535353";
    ctx.lineWidth = 10;
    ctx.arc(250, 250, 100, 0, 2 * Math.PI);
    ctx.stroke();

    ctx.beginPath();
    ctx.lineWidth = 1;
    ctx.arc(250, 250, 240, 0, 2 * Math.PI);
    ctx.stroke();

    //! Text info
    ctx.font = "700 64px Open Sans";
    ctx.textAlign = "center";
    ctx.fillText(Math.round( rpm*10 * 10 ) / 10, 250, 235);

    ctx.font = "700 15px Open Sans";
    ctx.fillText("deg C", 250, 255);

    ctx.stroke();
    ctx.font = "700 64px Open Sans";
    ctx.textAlign = "center";
    ctx.fillText(speed, 250, 315);

    ctx.font = "700 15px Open Sans";
    ctx.fillText("H %", 250, 335);


    ctx.fillStyle = "#999";
    ctx.font = "700 32px Open Sans";
    ctx.fillText("test", 250, 460);


    ctx.fillStyle = "#FFF";
    for (var i = 10; i <= Math.ceil(topSpeed / 20) * 20; i += 10) {
        drawMiniNeedle(calculateSpeedAngle(
            i / topSpeed, 83.07888, 34.3775) * Math.PI,
            i % 20 == 0 ? 3 : 1,
            i%20 == 0 ? i : '');

        if(i<=100) {
            drawMiniNeedle(calculateSpeedAngle(
                i / 47, 0, 22.9183) * Math.PI,
                i % 20 == 0 ? 3 : 1,
                i % 10 == 0 ? i / 1 : '');
        }
    }

    ctx.beginPath();
    ctx.strokeStyle = "#41dcf4";
    ctx.lineWidth = 25;
    ctx.shadowBlur = 20;
    ctx.shadowColor = "#00c6ff";

    ctx.strokeStyle = speedGradient;
    ctx.arc(250, 250, 228, .6 * Math.PI, calculateSpeedAngle(speed / topSpeed, 83.07888, 34.3775) * Math.PI);
    ctx.stroke();
    ctx.beginPath();
    ctx.lineWidth = 25;
    ctx.strokeStyle = rpmGradient;
    ctx.shadowBlur = 20;
    ctx.shadowColor = "#f7b733";
    ctx.arc(250, 250, 228, .4 * Math.PI, calculateRPMAngle(rpm / 4.7, 0, 22.9183) * Math.PI,true);

    // ctx.arc(250, 250, 228, .4 * Math.PI, calculateRPMAngle(rpm / 4.7, 0, 22.9183) * Math.PI, true);
    ctx.stroke();
    ctx.shadowBlur = 0;


    ctx.strokeStyle = '#41dcf4';

    speedNeedle(calculateSpeedAngle(speed / topSpeed, 83.07888, 34.3775) * Math.PI);

    ctx.strokeStyle = rpmGradient;
    rpmNeedle(calculateRPMAngle(rpm / 4.7, 0, 22.9183) * Math.PI);

    ctx.strokeStyle = "#000";

}


function setSpeed (s,r) {

    let speedM = parseFloat(s);
    let gear = 2;
    let rpm = parseFloat(r)/100;


    if (servRpm !== rpm && servSpeed !== speedM){
        servRpm = rpm;
        servSpeed = speedM;
        drawSpeedo(speedM,gear,rpm,100);
    }

}
function animateData(data,iniData){
    let t= ParseFloat(data[1])/100;
    let h = Math.floor(parseFloat(data[0]));
    let lt = arseFloat(iniData[1])/100;
    let lh = Math.floor(parseFloat(iniData[0]));

    let tflag,hflag ;
    tflag = false;
    hflag = false;


    let interval = setInterval(function(){
        if (!tflag){
            if (lt >= t && !tflag){
                lt -= 0.01;
                if (lt<=t)tflag = true;
            }
            if (lt <= t && !tflag){
                lt += 0.01;
                if (lt>=t)tflag = true;
            }
        }
        if(!hflag){
            if (h<=lh && !hflag){
                lh--;
                if (h >= lh) hflag = true;
            }
            if (h >= lh && !hflag){
                lh++;
                if (h <= lh) hflag = true;
            }
        }



        drawSpeedo(lh,gear,lt,100);
        if (hflag && tflag){
            clearInterval(interval);
        }
    }, 60);
}



document.addEventListener('DOMContentLoaded', function() {

    //setInterval(setSpeed, 2000)
    //renderCanvas();
    // setSpeed(,0);
    // drawSpeedo(120,4,.8,160);

    let iniData = [0,0];
    animateData([0,0],iniData);


    $.getJSON("http://90.126.144.79/public/script/php/temperature.php",function(result){
        animateData(result,iniData);
        iniData = result;
            setTimeout(function run() {
                $.ajax({
                    dataType: 'json',
                    url: "http://90.126.144.79/public/script/php/temperature.php",
                    success: function(result){
                        animateData(result,iniData);
                    }});
                setTimeout(run, 3000);
            }, 3000);
        })

}, false);























