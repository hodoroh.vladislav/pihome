class Chart{
    constructor(){
        this.url = 'http://192.168.1.14/chart/public/script/getChart.php';
        this.dataType = 'json';
        this.method = 'POST';
        this.data = [];
        this.time = "day";
        this.chartStartColor = Highcharts.Color(Highcharts.getOptions().colors[3]).setOpacity(0.1).get('rgba');

            // this.chartGetData();
        // this.draw(this.data)
       this.start();
    }

    setTime(time){
        this.time = time;
    }
    getTimeObject(){
        return {time:this.time};
    }
    start(){
        $.when(
            this.chartGetData({home:"home"},this.getTimeObject()),
        ).then(function (a1) {
            chart.draw(
                Chart.parseChart(a1,'humidity_v'),
                Chart.parseChart(a1,'temperature_v'),
            );
        })
    }
    static parseChart(data,value){
        let tmp = [];
        $.each(data, function(i, val) {
                let arr = [new Date(val['date']).getTime(),parseFloat(val[value])];
                tmp.push(arr);
        });
        return tmp;


        // new Date(data[0].date).getTime()
    }


    draw(data1,data2){
        // create the chart
        Highcharts.setOptions({
            global: {
                useUTC: false
            }
        });
        Highcharts.stockChart('chart_place', {


            title: {
                text: 'Chart Temp'
            },
            // useUTC:false,
            // subtitle: {
            //     text: 'Using ordinal X axis'
            // },

            // xAxis: {
            //     gapGridLineWidth: 0
            // },
            xAxis: {
                type: 'datetime',
                dateTimeLabelFormats: {
                    second: '<br/>%H:%M:%S',
                    minute: '<br/>%H:%M:%S',
                    hour: '<br/>%H:%M:%S',
                    day: '%Y<br/>%b-%d',
                    week: '%Y<br/>%b-%d',
                    month: '%Y-%b',
                    year: '%Y'
                },
                pointInterval : 4000,
                minTickInterval: 60000
            },
            yAxis:[{
                    title: {
                        text: 'Temperature (°C)'
                    },
                    height: "45%",
                    lineWidth: 2
                },{
                    title: {
                    text: 'humidity'
                    },
                    top: "50%",
                    height: "35%",
                    offset: 0,
                    lineWidth: 2
                },
                ],

            rangeSelector: {
                buttons: [{
                    type: 'hour',
                    count: 1,
                    text: '1h'
                }, {
                    type: 'day',
                    count: 1,
                    text: '1D'
                }, {
                    type: 'all',
                    count: 1,
                    text: 'All'
                }],
                selected: 1,
                inputEnabled: true
            },

            series: [{
                name: 'Temperature',
                color:'#FF5722',
                type: 'area',
                data: data2,
                gapSize: 2,
                tooltip: {
                    valueDecimals: 2
                },
                    // color: '#FF0000',
                fillColor: {
                    linearGradient: {
                        x1: 0,
                        y1: 0,
                        x2: 0,
                        y2: 1
                    },
                    stops: [
                        [0, Highcharts.Color(Highcharts.getOptions().colors[5]).setOpacity(0.1).get('rgba')],
                        [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                    ]
                },
                threshold: false
            },
                {
                    name: 'Humidity',
                    type: 'area',
                    data: data1,
                    yAxis:1,
                    color:'#3acfff',
                    gapSize: 2,
                    tooltip: {
                        valueDecimals: 2
                    },
                    fillColor: {
                        linearGradient: {
                            x1: 0,
                            y1: 0,
                            x2: 0,
                            y2: 1
                        },
                        stops: [
                            [0, Highcharts.Color(Highcharts.getOptions().colors[7]).setOpacity(0.1).get('rgba')],
                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                        ]
                    },
                    threshold: false
                }
            ]
        });
    }

    chartGetData(data,time){
        let tmp = [];
        return $.ajax({
            url: this.url,
            data:Object.assign(data,time),
            dataType : this.dataType,
            method: this.method,
            success:
                function (data, textStatus) {
                    // $.each(data, function(i, val) {
                    //     // chart.parseChart(i,val,tdata);
                    //     let arr = [new Date(val['date']).getTime(),parseFloat(val['value'])];
                    //     // chart.data.push(arr);
                    //     tmp.push(arr);
                    // });
                    // console.log(tmp);
                }




        });
        // this.draw(this.data);
    }
}
let chart = new Chart();

$(".day").on("click",function () {
chart.setTime("day");
chart.start();

});

$(".week").on("click",function () {
    chart.setTime("week");
    chart.start();

});

$(".month").on("click",function () {
    chart.setTime("month");
    chart.start();

});

$(".alltime").on("click",() => {
    chart.setTime("alltime");
    chart.start();
});


// $.getJSON('https://cdn.rawgit.com/highcharts/highcharts/057b672172ccc6c08fe7dbb27fc17ebca3f5b770/samples/data/new-intraday.json', function (data) {
//     console.log(data);
// })