<?php

use Src\App\App;
use Src\Core\Html\Html;

define('ROOT',dirname(__DIR__));
require ROOT. '/vendor/autoload.php';

$page = 'index';

if (isset($_GET['page']))
    $page = $_GET['page'];


ob_start();

$nav = Html::getInstance()->getNav([
    'Home'=> '?page=index',
    'Chart' => '?page=chart.chart',
    'Relay' => '?page=relay.relay'
]);
require App::getInstance()->getPath($page);


$content = ob_get_clean();

require ROOT . '/page/templates/default.php';