<?php
use Src\Core\Html\Html;


?>



<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>piHome</title>

    <!-- Bootstrap core CSS-->
    <!--        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">-->
    <!-- Custom fonts for this template-->
<!--    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">-->

    <!-- Page level plugin CSS-->



    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link href="css/simple-sidebar.css" rel="stylesheet">

    <?=Html::getInstance()->getCSS();?>
    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

</head>

<body id="page-top"  class="<?php // ($_SESSION['sidebar'])?'sidebar-toggled':'';?>" >

<div id="wrapper">
    <?= $nav?>
    <a href="#menu-toggle" class="btn btn-secondary mb-3 btn-custom" id="menu-toggle">Toggle Menu</a>


    <div id="content-wrapper mt-3">

        <div class="<?=($page = 'chart.chart')?'container-fluid p-0':'container';?>">
            <?= Html::getInstance()->getBreadcrumb($page)?>

            <?= $content?>
<!--            <a href="#menu-toggle" class="btn btn-secondary" id="menu-toggle">Toggle Menu</a>-->

        </div>



        <footer class="sticky-footer">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright © rPi - Chart</span>
                </div>
            </div>
        </footer>

    </div>
</div>
<!-- /#wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<?//= HTML::getLogout();?>

<!--  core JavaScript-->







<!-- Core plugin JavaScript-->

<?= Html::getInstance()->getScript();?>

<!-- Custom scripts for all pages-->

<script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
</script>



<script type="text/javascript">

    // let sidebar =$('ul.sidebar');
    // let sidebarProcess = new SidebarStatus(sidebar);
    // $('#sidebarToggle').click(function () {
    //     sidebarProcess.toggled();
    // })
    ;
</script>




</body>

</html>



