<?php

use Src\App\App;
use \Src\Core\Html\Html;

error_reporting(E_ALL);
ini_set('display_errors', 1);

Html::getInstance()->setScript([
    'script/js/relay' => ['relaySwitch.js']
]);
?>



<table class="table table-light">
    <thead>
    <th scope="col">Relay</th>
    <th scope="col">Description</th>
    <th scope="col">State</th>
    </thead>
    <tbody>
        <?php foreach (App::getInstance()->getTable('Relay')->all() as $post):?>
            <tr scope="row">
                <td ><?=$post->id?></td>
                <td><?=$post->description?></td>
                <td>
                    <button data-userId="<?= $post->id?>" class="btn w-100 relay-btn btn-success <?php if ($post->state === '0')echo "btn-danger"?>">State</button>
                </td>
            </tr>
        <?php endforeach;?>
    </tbody>
</table>



