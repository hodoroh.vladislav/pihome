<?php

use Src\Core\Html\Html;

Html::getInstance()->setScript(
        array(
//            'other' => array('https://cdnjs.cloudflare.com/ajax/libs/flot.tooltip/0.9.0/jquery.flot.tooltip.min.js'),
//            'script/js/chart' => array('highstock.js'),
//            'script/js/chart/modules' => array('exporting.js','export-data.js'),
//            'script/js/chart' => array('chart.js')

        ));
Html::getInstance()->setCSS([
    [
        'path'=>'css',['master.css']
    ]])
?>

<div id="chart_place" style="height: 100vh; width: 100%"></div>

<div class="buttons">
    <div class="day  button "><span>day</span></div>
    <div class="week  button "><span>week</span></div>
    <div class="month  button "><span>month</span></div>
    <div class="alltime  button "><span>all time</span></div>

</div>


<script src="script/js/chart/highstock.js"></script>
<script src="script/js/chart/modules/exporting.js"></script>
<script src="script/js/chart/modules/export-data.js"></script>

<script src="script/js/chart/chart.js"></script>