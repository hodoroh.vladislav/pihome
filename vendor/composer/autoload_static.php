<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitdf7c45ccfa1ad16f4e027ea5d3b629b9
{
    public static $prefixLengthsPsr4 = array (
        'S' => 
        array (
            'Src\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Src\\' => 
        array (
            0 => __DIR__ . '/../..' . '/Src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitdf7c45ccfa1ad16f4e027ea5d3b629b9::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitdf7c45ccfa1ad16f4e027ea5d3b629b9::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
