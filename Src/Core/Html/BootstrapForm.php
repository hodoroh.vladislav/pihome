<?php

namespace Src\Core\Html;

class BootstrapForm extends Form
{

    protected function surround($html)
    {
        return '<div class="form-group">'. $html .'</div>';
    }

    public function input($name,$label,$options = [])
    {
        $other = isset($options['other'])?$options['other']:'';
        $type = isset($options['type'])? $options['type']: 'text';
        $nLabel ='<label class="font-weight-bold">'.ucfirst($label). '</label>';
//        var_dump($type);
        if ($type === 'textarea')
            $input = '<textarea class="form-control" style="height:250px;" name="'.$name.'">'.$this->getValue($name).'</textarea>';
        else
            $input = '<input type="'.$type.'" '.$other.' name="'.$name.'" class="form-control" value="'.$this->getValue($name).'" placeholder="Enter  '.$name.'">';

        $html = $nLabel.$input;
        return $this->surround(
            $html
        );
    }

    public function submit()
    {
        return $this->surround('<button type="submit" class="btn btn-secondary">Envoyer</button>');
    }

}