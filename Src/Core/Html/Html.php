<?php

namespace Src\Core\Html;


class Html{

    private  $script = '';
    private  $style;
    private static $activeElement;
    private static $tableColumns;

    private static $_instance;

    public static function getInstance(){
        if (self::$_instance === null){
            self::$_instance = new Html();
            return self::$_instance;
        }
        return self::$_instance;
    }
    public function getBreadcrumb($page){
        $breadcrumb = '';
        switch ($page){
            case 'index':
                $breadcrumb = "Home";
                break;
            case 'relay.relay';
                $breadcrumb = "Relay Control";
                break;
        }
        return $this->surround("h1",$breadcrumb,2);
    }
    public  function getNav($param)
    {
        return '
            <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li class="sidebar-brand">
                    <a href="#">
                        Start Bootstrap
                    </a>
                </li>
               '.$this->procNav($param).'
            </ul>
        </div>';
    }
    private function procNav($param){
        $html = '';
        foreach ($param as $name => $url){
            $html .= '<li>
                    <a href="'.$url.'">'.$name.'</a>
                </li>';
        }
        return $html;
    }


    public function setScript($config){

        foreach ($config as $path => $scripts){
            if ($path === 'other'){
                foreach ($scripts as $script){
                    $this->script .= '<script src="'.$script.'"> </script>';

                }
            }else{
                foreach ($scripts as $script){
                    $this->script .= '<script src="'.$path.'/'.$script.'"> </script>';
                }
            }
        }


    }
    public function getScript(){
        return $this->script;
    }
    public function setCSS($array = []){
        $scripts = '';
        foreach ($array as $value){
            if ($value['path'] !== 'other'){
                foreach ($value[0] as $script){
                    $scripts .='<link rel="stylesheet" href="'.$value['path'].'/'.$script.'">';
                }
            }else{
                foreach ($value[0] as $script){
                    $scripts .='<link rel="stylesheet" href="'.$script.'">';
                }
            }
        }
//        var_dump($scripts);
        $this->style = $scripts;

    }
    public
    function getCSS(){
        return $this->style;
    }


    public  function surround($tag, $html,$method = 1)
    {
        if ($method === 1) {
            return str_replace('{HTML}', $html, $tag);
        }
        else if ($method === 2) {
            return '<' . $tag . '>' . $html . '</' . $tag . '>';
        }
    }








    public static function getButtons(){
        return '
        <div class="buttons">
            <div class="day  button "><span>day</span></div>
            <div class="week  button "><span>week</span></div>
            <div class="month  button "><span>month</span></div>
            <div class="alltime  button "><span>all time</span></div>

        </div>
';
    }
}