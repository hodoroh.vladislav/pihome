<?php


namespace Src\Core\Sensor;


class Sensor
{
    private $data = [];

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $value
     */
    private function msleep($time)
    {
        usleep($time * 1000000);
    }
    private function getTemp($path,$script){
        $commands = "sudo {$path}/./{$script}";
        exec($commands,$output);
        preg_match_all("/[0-9]*\.[0-9]/", current($output), $output_array);
        return $output_array;
    }
    public function test(){
        var_dump(__DIR__);
        var_dump(dirname(__DIR__));
    }
    public function parseJson($data){
        return json_encode($data);
    }

    public function exec($config){
//        $commands = 'sudo '.dirname(__DIR__)."/C/"."./fdht";
        $path = $config['path'];
        $script = $config['script'];

        $this->data = $this->getTemp($path,$script);
        $state = !$this->data[0];
        if (!$state){
            return current($this->data);
        }else{
            while ($state){
                $this->msleep(0.3);
                $this->data=$this->getTemp($path,$script);
                $state = !$this->data[0];
            }

            return current($this->data);

        }

    }


}